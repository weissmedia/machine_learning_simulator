import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg

import pandas as pd

import tkinter as tk
from tkinter import *

import mglearn
from mglearn.datasets import make_forge

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.datasets import make_moons, load_breast_cancer, load_iris
from sklearn.preprocessing import MinMaxScaler


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master.title('Machine Learning')
        self.figure = None
        self.top_frame = None
        self.bar_frame_mpl = None
        self.bar_frame_neighbors = None
        self.plot_frame = None
        self.canvas = None

        self.fields = dict(solver=StringVar(value='adam'),
                           activation=StringVar(value='relu'),
                           layer=IntVar(value=1),
                           neurons=IntVar(value=100),
                           alpha=StringVar(value=0.0001),
                           random_state=IntVar(value=0),
                           max_iter=IntVar(value=200))

        self.fields_neighbors = dict(weights=StringVar(value='uniform'),
                                     n_neighbors=IntVar(value=5))

        self.grid(sticky=tk.W + tk.E + tk.N + tk.S)

        self.create_menu()
        #self.start_screen()

    def start_screen(self):
        self.top_frame = tk.Frame(self.master)
        self.top_frame.grid()

        self.figure = plt.figure()
        ax = self.figure.gca()

        bar_frame = tk.Frame(self.top_frame, relief=GROOVE, bd=0)
        bar_frame.grid()
        w = Label(bar_frame, text="MASCHINE LEARNING")
        w.grid()

        plot_frame = tk.Frame(self.top_frame)
        plot_frame.grid()

        iris_dataset = load_iris()

        X_train, X_test, y_train, y_test = train_test_split(
            iris_dataset['data'], iris_dataset['target'], random_state=0)

        iris_dataframe = pd.DataFrame(X_train)
        pd.plotting.scatter_matrix(iris_dataframe, c=y_train, marker='o', hist_kwds={'bins': 20},
                                   s=60, alpha=.8, cmap=mglearn.cm3, ax=ax)

        canvas = FigureCanvasTkAgg(self.figure, master=plot_frame)
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        canvas.show()

    def create_menu(self):
        menu = Menu(self.master)

        self.master.config(menu=menu)

        item = Menu(menu)
        item.add_command(label="Exit", command=self.on_exit)
        menu.add_cascade(label="File", menu=item)

        item = Menu(menu)

        item.add_command(label="Tuning N-Neighbors (k-NN)...",
                         command=lambda: self.neighbors(self.neighbors_plot))

        item.add_command(label="Tuning Multilayer Perceptron (MLP)...", command=lambda: self.mlp(self.mlp_plot))

        #item.add_command(label="Analysis N-Neighbors (k-NN)...",
        #                 command=lambda: self.neighbors(self.neighbors_breast_cancer_plot))

        #item.add_command(label="Analysis Multilayer Perceptron (MLP)...",
        #                 command=lambda: self.mlp(self.mlp_breast_cancer_plot, tk.W))

        item.add_command(label="Comparison MLP and k-NN...", command=self.comparison)

        menu.add_cascade(label="Tools", menu=item)

    def comparison(self):
        self.mlp(self.mlp_breast_cancer_plot, tk.W)
        self.neighbors(lambda: self.neighbors_breast_cancer_plot(sticky=tk.E), tk.E+tk.N, not_destroy=True)

    def mlp(self, plotter, sticky=None):
        if self.top_frame:
            self.top_frame.destroy()

        if self.bar_frame_neighbors:
            self.bar_frame_neighbors.destroy()

        if self.bar_frame_mpl:
            self.bar_frame_mpl.destroy()

        if not sticky:
            sticky = tk.N

        self.top_frame = tk.Frame(self.master, width=640).grid(row=0, columnspan=3)
        self.bar_frame_mpl = tk.Frame(self.top_frame, relief=GROOVE, bd=3)
        self.bar_frame_mpl.grid(row=0, column=0, rowspan=5, columnspan=6, sticky=sticky)

        row, column = 0, 0

        solver = ('lbfgs','sgd', 'adam')
        Label(self.bar_frame_mpl, text='Solver').grid(row=row, column=column, sticky=tk.W, padx=8)

        for index, txt in enumerate(solver):
            Radiobutton(self.bar_frame_mpl,
                        text=txt,
                        width=7,
                        justify=LEFT,
                        variable=self.fields['solver'],
                        value=txt).grid(row=row+index+1, column=column, sticky=tk.W, padx=8)

        column += 1
        activation = ('identity', 'logistic', 'tanh', 'relu')
        Label(self.bar_frame_mpl, text='Activation').grid(row=row, column=column, sticky=tk.W)

        for index, txt in enumerate(activation):
            Radiobutton(self.bar_frame_mpl,
                        text=txt,
                        variable=self.fields['activation'],
                        width=9,
                        justify=LEFT,
                        value=txt).grid(row=row+index+1, column=column, sticky=tk.W)

        column += 1
        Label(self.bar_frame_mpl, text='Layer', justify=LEFT).grid(row=row, column=column, sticky=tk.W, padx=8)
        Entry(self.bar_frame_mpl, width=5, text=self.fields['layer'], justify=RIGHT).grid(row=row+1, column=column, sticky=tk.W, padx=8)

        Label(self.bar_frame_mpl, text='Neurons', justify=LEFT).grid(row=row+2, column=column, sticky=tk.W, padx=8, rowspan=2)
        Entry(self.bar_frame_mpl, width=5, text=self.fields['neurons'], justify=RIGHT).grid(row=row+3, column=column, sticky=tk.W, padx=8, rowspan=2)

        column += 1
        Label(self.bar_frame_mpl, text='Alpha', justify=LEFT).grid(row=row, column=column, sticky=tk.W)
        Entry(self.bar_frame_mpl, width=6, text=self.fields['alpha'], justify=RIGHT).grid(row=row+1, column=column, sticky=tk.W)

        #Label(bar_frame, text='Random', justify=LEFT).grid(row=row+2, column=column, sticky=tk.W, rowspan=2)
        #Entry(bar_frame, width=6, text=self.fields['random_state'], justify=RIGHT).grid(row=row+3, column=column, sticky=tk.W, rowspan=2)

        Label(self.bar_frame_mpl, text='Max-Iter', justify=LEFT).grid(row=row + 2, column=column, sticky=tk.W, rowspan=2)
        Entry(self.bar_frame_mpl, width=6, text=self.fields['max_iter'], justify=RIGHT).grid(row=row + 3, column=column,
                                                                                    sticky=tk.W, rowspan=2)

        Button(self.bar_frame_mpl, text='Plot', width=30, command=plotter).grid(row=5, column=0, columnspan=6)

    def mlp_plot(self):
        self.figure = plt.figure()
        ax = self.figure.gca()

        if self.plot_frame:
            self.plot_frame.destroy()

        self.plot_frame = tk.Frame(self.top_frame)
        self.plot_frame.grid(row=6, column=0)

        solver = self.fields['solver'].get()
        activation = self.fields['activation'].get()
        layer = int(self.fields['layer'].get())
        neurons = int(self.fields['neurons'].get())
        alpha = float(self.fields['alpha'].get())
        random_state = int(self.fields['random_state'].get())

        hidden_layer_sizes = [neurons for _ in range(layer)]

        X, y = make_moons(n_samples=100, noise=0.25, random_state=3)

        X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=42)

        mlp = MLPClassifier(solver=solver,
                            activation=activation,
                            random_state=random_state,
                            hidden_layer_sizes=hidden_layer_sizes,
                            alpha=alpha)

        mlp.fit(X_train, y_train)

        mglearn.plots.plot_2d_separator(mlp, X_train, fill=True, alpha=.3, ax=ax)
        mglearn.discrete_scatter(X_train[:, 0], X_train[:, 1], y_train, ax=ax)

        plt.xlabel('Merkmal 0')
        plt.ylabel('Merkmal 1')

        canvas = FigureCanvasTkAgg(self.figure, master=self.plot_frame)
        canvas.get_tk_widget().pack()
        canvas.show()

    def mlp_breast_cancer_plot(self):
        self.figure = plt.figure()
        ax = self.figure.gca()
        self.plot_frame = tk.Frame(self.top_frame)
        self.plot_frame.grid(row=6, column=0, sticky=tk.W)

        solver = self.fields['solver'].get()
        activation = self.fields['activation'].get()
        layer = int(self.fields['layer'].get())
        neurons = int(self.fields['neurons'].get())
        alpha = float(self.fields['alpha'].get())
        random_state = int(self.fields['random_state'].get())
        max_iter = int(self.fields['max_iter'].get())

        hidden_layer_sizes = [neurons for _ in range(layer)]

        cancer = load_breast_cancer()
        X_train, X_test, y_train, y_test = train_test_split(cancer.data, cancer.target,
                                                            stratify=cancer.target, random_state=0)

        scaler = MinMaxScaler()
        scaler.fit(X_train)

        X_train_scaled = scaler.transform(X_train)
        X_test_scaled = scaler.transform(X_test)

        mlp = MLPClassifier(random_state=random_state,
                            solver=solver,
                            activation=activation,
                            hidden_layer_sizes=hidden_layer_sizes,
                            alpha=alpha,
                            max_iter=max_iter)

        mlp.fit(X_train_scaled, y_train)

        score_train = mlp.score(X_train_scaled, y_train)
        Label(self.plot_frame, text='Genauigkeit auf den Trainingsdaten: {:.3f}'.format(score_train)).grid(sticky=tk.W)

        score_test = mlp.score(X_test_scaled, y_test)
        Label(self.plot_frame, text='Genauigkeit auf den Testdaten: {:.3f}'.format(score_test)).grid(sticky=tk.W)

        Label(self.plot_frame, text='Abweichung: {:.3f}'.format(score_train - score_test)).grid(sticky=tk.W)

    def neighbors(self, plotter, sticky=None, **kwargs):
        if self.top_frame:
            self.top_frame.destroy()

        if self.bar_frame_neighbors:
            self.bar_frame_neighbors.destroy()

        if not kwargs.get('not_destroy') and self.bar_frame_mpl:
            self.bar_frame_mpl.destroy()

        if not sticky:
            sticky = tk.N

        self.top_frame = tk.Frame(self.master, width=640).grid(row=0, columnspan=3)
        self.bar_frame_neighbors = tk.Frame(self.top_frame, relief=GROOVE, bd=3)
        self.bar_frame_neighbors.grid(row=0, column=0, rowspan=5, columnspan=6, sticky=sticky)

        row, column = 0, 0

        weights = ('uniform','distance')
        Label(self.bar_frame_neighbors, text='Weights').grid(row=row, column=column, sticky=tk.W, padx=8)

        for index, txt in enumerate(weights):
            Radiobutton(self.bar_frame_neighbors,
                        text=txt,
                        width=9,
                        justify=LEFT,
                        variable=self.fields_neighbors['weights'],
                        value=txt).grid(row=row+index+1, column=column, sticky=tk.W, padx=8)

        column += 1
        Label(self.bar_frame_neighbors, text='Neighbors', justify=LEFT).grid(row=row, column=column, sticky=tk.W, padx=8)
        Entry(self.bar_frame_neighbors, width=5, text=self.fields_neighbors['n_neighbors'], justify=RIGHT).grid(row=row+1, column=column, sticky=tk.W, padx=8)

        Button(self.bar_frame_neighbors, text='Plot', width=30, command=plotter).grid(row=5, column=0, columnspan=6)

    def neighbors_plot(self):
        self.figure = plt.figure()
        ax = self.figure.gca()
        plot_frame = tk.Frame(self.top_frame)
        plot_frame.grid(row=6, column=0)

        weights = self.fields_neighbors['weights'].get()
        n_neighbors = int(self.fields_neighbors['n_neighbors'].get())

        X, y = make_forge()
        clf = KNeighborsClassifier(weights=weights, n_neighbors=n_neighbors).fit(X, y)
        mglearn.plots.plot_2d_separator(clf, X, fill=True, eps=0.5, ax=ax, alpha=.4)
        mglearn.discrete_scatter(X[:, 0], X[:, 1], y, ax=ax)

        canvas = FigureCanvasTkAgg(self.figure, master=plot_frame)
        canvas.get_tk_widget().pack()
        canvas.show()

    def __neighbors_breast_cancer_plot(self):
        self.figure = plt.figure()
        plot_frame = tk.Frame(self.top_frame)
        plot_frame.grid(row=6, column=0)

        cancer = load_breast_cancer()
        X_train, X_test, y_train, y_test = train_test_split(cancer.data, cancer.target,
                                                            stratify=cancer.target, random_state=66)

        weights = self.fields_neighbors['weights'].get()
        n_neighbors = int(self.fields_neighbors['n_neighbors'].get())

        training_accuracy = []
        test_accuracy = []
        neighbors_setting = range(1, n_neighbors)

        for n_neighbors in neighbors_setting:
            clf = KNeighborsClassifier(weights=weights, n_neighbors=n_neighbors).fit(X_train, y_train)
            training_accuracy.append(clf.score(X_train, y_train))
            test_accuracy.append(clf.score(X_test, y_test))

        plt.plot(neighbors_setting, training_accuracy, label='Genauigkeit Trainingsdaten')
        plt.plot(neighbors_setting, test_accuracy, label='Genauigkeit Testdaten')
        plt.ylabel('Genauigkeit')
        plt.xlabel('n_neighbors')
        plt.legend()

        canvas = FigureCanvasTkAgg(self.figure, master=plot_frame)
        canvas.get_tk_widget().pack()
        canvas.show()

    def neighbors_breast_cancer_plot(self, sticky=None):
        self.figure = plt.figure()
        self.plot_frame = tk.Frame(self.top_frame)

        if sticky:
            sticky = tk.W

        self.plot_frame.grid(row=6, column=2, columnspan=3, sticky=sticky)

        cancer = load_breast_cancer()
        X_train, X_test, y_train, y_test = train_test_split(cancer.data, cancer.target,
                                                            stratify=cancer.target, random_state=66)

        weights = self.fields_neighbors['weights'].get()
        n_neighbors = int(self.fields_neighbors['n_neighbors'].get())

        clf = KNeighborsClassifier(weights=weights, n_neighbors=n_neighbors).fit(X_train, y_train)

        score_train = clf.score(X_train, y_train)
        Label(self.plot_frame, text='Genauigkeit auf den Trainingsdaten: {:.3f}'.format(score_train)).grid(sticky=sticky)

        score_test = clf.score(X_test, y_test)
        Label(self.plot_frame, text='Genauigkeit auf den Testdaten: {:.3f}'.format(score_test)).grid(sticky=sticky)

        Label(self.plot_frame, text='Abweichung: {:.3f}'.format(score_train-score_test)).grid(sticky=sticky)

    def on_exit(self):
        self.quit()


root = tk.Tk()
root.resizable(width=FALSE, height=FALSE)
root.geometry('650x650')
app = Application(master=root)
app.mainloop()